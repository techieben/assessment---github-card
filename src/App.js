import React, { Component } from "react";

import { Card, Image, Button, Input } from "semantic-ui-react";

import "./App.css";

let userName = "";

const GITHUB_USER_INFO = `https://api.github.com/users/`;

class App extends Component {
  state = {
    active: false,
    user: {}
  };

  handleClick = event => {
    userName = document.getElementById("input").value;
    fetch(GITHUB_USER_INFO + userName)
      .then(response => response.json())
      .then(user => {
        user.login
          ? this.setState({ user, active: true })
          : alert("No Bounty Found");
      });
  };

  render() {
    return (
      <React.Fragment>
        <Input id="input" type="text" placeholder="Github User Name"></Input>
        <Button onClick={this.handleClick}>Find Bounty</Button>
        {this.state.active && (
          <Card>
            <Card.Content>
              <Card.Header>WANTED</Card.Header>
            </Card.Content>
            <Image alt="user" src={this.state.user.avatar_url} />
            <Card.Content>
              <Card.Header>{this.state.user.name}</Card.Header>
              <Card.Description>
                ${this.state.user.public_repos},000,000 REWARD
              </Card.Description>
              <Card.Meta>Bounty ID: {this.state.user.id}</Card.Meta>
              <Card.Meta>Convicted: {this.state.user.created_at}</Card.Meta>
              <Card.Meta>Last Seen: {this.state.user.updated_at}</Card.Meta>
              <Card.Meta>
                Connascence Index: {this.state.user.following}
              </Card.Meta>
              <Card.Meta>
                Critical Vulnerabilities: {this.state.user.followers}
              </Card.Meta>
              <Card.Description>Wanted for:</Card.Description>
              <Card.Meta>{this.state.user.bio}</Card.Meta>
            </Card.Content>
            <Card.Content extra>
              Please contact your local GitHub authority for contract details or
              upon suspect apprehension. No Disintegrations.
            </Card.Content>
          </Card>
        )}
      </React.Fragment>
    );
  }
}

export default App;
